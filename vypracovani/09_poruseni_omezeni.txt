-- 1 --
INSERT INTO t_user VALUES ('io@test.cz', 'paaaasssswww', 'Michael', 'Bush');
INSERT INTO t_practiceday VALUES (to_date('22.01.20','DD.MM.RR'),'-1','0','io@test.cz'); 
INSERT INTO t_practiceday VALUES (to_date('22.01.20','DD.MM.RR'),'0','-1','io@test.cz'); 

-- 2 --
INSERT INTO t_user VALUES ('iotest.cz', 'paaaasssswww', 'Michael', 'Bush');
INSERT INTO t_user VALUES ('io@testcz', 'paaaasssswww', 'Michael', 'Bush');
INSERT INTO t_user VALUES ('iotestcz', 'paaaasssswww', 'Michael', 'Bush');


-- 3 --
BEGIN
    vocabulary_api.delete_language('ic');

    INSERT INTO t_language VALUES ('ic', 'testIC');
    INSERT INTO t_wordlist VALUES (80500, 'test ic', 'testing integrity constraints', 'ic', 'en');
    
    FOR ind IN 1..60
    LOOP
        INSERT INTO T_WORD VALUES (80000 + ind, 'test', 'ic');
        INSERT INTO t_includedword VALUES (80000 + ind, 80500);
    END LOOP;

    INSERT INTO T_WORD VALUES (80000 + 61, 'test', 'ic');
    INSERT INTO t_includedword VALUES (80000 + 61, 80500);
END;
