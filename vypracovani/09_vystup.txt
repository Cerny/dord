-- 1 --
1 row inserted.


Error starting at line : 2 in command -
INSERT INTO t_practiceday VALUES (to_date('22.01.20','DD.MM.RR'),'-1','0','io@test.cz')
Error report -
ORA-20222: Correct count and wrong count have to be positive numbers
ORA-06512: at "DOKRALJA3.PRACTICE_DAY_COUNTS_IC_TRIGGER", line 4
ORA-04088: error during execution of trigger 'DOKRALJA3.PRACTICE_DAY_COUNTS_IC_TRIGGER'


Error starting at line : 3 in command -
INSERT INTO t_practiceday VALUES (to_date('22.01.20','DD.MM.RR'),'0','-1','io@test.cz')
Error report -
ORA-20222: Correct count and wrong count have to be positive numbers
ORA-06512: at "DOKRALJA3.PRACTICE_DAY_COUNTS_IC_TRIGGER", line 4
ORA-04088: error during execution of trigger 'DOKRALJA3.PRACTICE_DAY_COUNTS_IC_TRIGGER'


-- 2 --
Error starting at line : 5 in command -
INSERT INTO t_user VALUES ('iotest.cz', 'paaaasssswww', 'Michael', 'Bush')
Error report -
ORA-20222: E-mail is not valid.
ORA-06512: at "DOKRALJA3.EMAIL_VALIDATION_IC_TRIGGER", line 4
ORA-04088: error during execution of trigger 'DOKRALJA3.EMAIL_VALIDATION_IC_TRIGGER'


Error starting at line : 6 in command -
INSERT INTO t_user VALUES ('io@testcz', 'paaaasssswww', 'Michael', 'Bush')
Error report -
ORA-20222: E-mail is not valid.
ORA-06512: at "DOKRALJA3.EMAIL_VALIDATION_IC_TRIGGER", line 4
ORA-04088: error during execution of trigger 'DOKRALJA3.EMAIL_VALIDATION_IC_TRIGGER'


Error starting at line : 7 in command -
INSERT INTO t_user VALUES ('iotestcz', 'paaaasssswww', 'Michael', 'Bush')
Error report -
ORA-20222: E-mail is not valid.
ORA-06512: at "DOKRALJA3.EMAIL_VALIDATION_IC_TRIGGER", line 4
ORA-04088: error during execution of trigger 'DOKRALJA3.EMAIL_VALIDATION_IC_TRIGGER'


-- 3 --
Error starting at line : 1 in command -
BEGIN
    vocabulary_api.delete_language('ic');

    INSERT INTO t_language VALUES ('ic', 'testIC');
    INSERT INTO t_wordlist VALUES (80500, 'test ic', 'testing integrity constraints', 'ic', 'en');

    FOR ind IN 1..60
    LOOP
        INSERT INTO T_WORD VALUES (80000 + ind, 'test', 'ic');
        INSERT INTO t_includedword VALUES (80000 + ind, 80500);
    END LOOP;

    INSERT INTO T_WORD VALUES (80000 + 61, 'test', 'ic');
    INSERT INTO t_includedword VALUES (80000 + 61, 80500);
END;
Error report -
ORA-20222: Reached max number of words in the word list.
ORA-06512: at "DOKRALJA3.WORDS_CNT_IN_WL_IC_TRIGGER", line 9
ORA-04088: error during execution of trigger 'DOKRALJA3.WORDS_CNT_IN_WL_IC_TRIGGER'
ORA-06512: at line 14

